import logging
import sys

logger = logging.getLogger(__name__)


class ScottyException(Exception):
    """Scotty standardexception."""

    pass


class ScottyQuietException(ScottyException):
    """Silent scotty exption."""

    quiet = True
    name = 'ScottyException'

    pass


class ExperimentException(ScottyException):
    """Exception for experiment."""

    pass


class WorkloadException(ScottyException):
    """Exception for workloads."""

    pass


class ResourceException(ScottyException):
    """Exception for resources."""

    pass


def quiet_hook(kind, message, traceback):
    """Hooks to silent traceback in exception."""
    if hasattr(kind, 'quiet') and kind.quiet:
        logger.error(f'{kind.name}: {message}')
    else:
        sys.__excepthook__(kind, message, traceback)


sys.excepthook = quiet_hook
