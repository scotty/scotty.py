import logging

from scotty.core import settings

logger = logging.getLogger(__name__)


class TelegrafResourceCollector(object):
    """Collector for resources based on telegraf."""

    def _load_setting_config(self):
        telegraf_conf = settings.load_content('telegraf', 'config')
        return telegraf_conf

    def _check_global_tag_section(self, telegraf_conf):
        exists = False
        if '[global_tags]' in telegraf_conf:
            exists = True
        return exists

    def create_config(self, exp_name, exp_uuid, resource_name):
        """Return telegraf config with global tags."""
        telegraf_conf = self._load_setting_config()
        global_tags = self._create_global_tags(exp_name, exp_uuid, resource_name)
        telegraf_conf = self._add_global_tags(telegraf_conf, global_tags)
        return telegraf_conf

    def _create_global_tags(self, exp_name, exp_uuid, resource_name):
        global_tags = ''
        global_tags += '[global_tags]\n'
        global_tags += f'  experiment_name = "{exp_name}"\n'
        global_tags += f'  experiment_uuid = "{exp_uuid}"\n'
        global_tags += f'  resource_name = "{resource_name}"'
        return global_tags

    def _add_global_tags(self, telegraf_conf, global_tags):
        if self._check_global_tag_section(telegraf_conf):
            telegraf_conf = telegraf_conf.replace('[global_tags]', global_tags, 1)
        else:
            telegraf_conf = f'{global_tags}\n\n{telegraf_conf}'
        return telegraf_conf
