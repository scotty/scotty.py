import logging

from influxdb import InfluxDBClient

from prettytable import PrettyTable

from scotty.core import settings

logger = logging.getLogger(__name__)


class ScottyInfluxDBClient(InfluxDBClient):
    """Class for influxdb communication."""

    def __init__(self):
        """Load client from config."""
        config = self._load_config()
        super().__init__(**config)

    def _load_config(self):
        config = {}
        config['username'] = settings.get('influxdb', 'username')
        config['password'] = settings.get('influxdb', 'password')
        config['database'] = settings.get('influxdb', 'database')
        config['port'] = settings.get('influxdb', 'port')
        config['host'] = settings.get('influxdb', 'host')
        config['ssl'] = settings.get('influxdb', 'ssl')
        config['verify_ssl'] = settings.get('influxdb', 'verify_ssl')
        return config


class InfluxDBExperimentData(object):
    """Class for experiment list from influxdb."""

    def __init__(self, client):
        """Bind influxdb client."""
        self._client = client

    def list_uuids_for_experiment(self, experiment):
        """Return uuids for experiment as resultset."""
        query = 'SHOW TAG VALUES FROM mem WITH KEY IN ("experiment_uuid")'
        query += f'WHERE "experiment_name" = \'{experiment}\''
        result = self._client.query(query)
        return result


class ExperimentDataFormater(object):
    """Formats influxdb results and print them."""

    @classmethod
    def get_exp_uuids_table(cls, result, experiment):
        """Return uuids for experiment as table."""
        table = PrettyTable()
        table.field_names = ["Experiment", "Experiment UUID"]
        table.add_row([experiment.name, ""])
        for experiment_uuid in result.get_points():
            table.add_row(["", experiment_uuid['value']])
        return table


class ExperimentDataExporter(object):
    """Exports experiment data from influxdb."""

    pass
