import os
import unittest

import mock

from scotty.core import settings


class SettingsTest(unittest.TestCase):
    """Testclass for core settings."""

    def setUp(self):
        """Run setup for setting tests."""
        self.maxDiff = None

    @mock.patch('scotty.core.settings.ConfigFile.path', new_callable=mock.PropertyMock)
    def test_telegraf_config_load(self, config_file_path):
        """Run test for collector injection in heat."""
        config_file_path.return_value = self._get_scotty_conf_path('scotty.conf')
        settings.load_settings()
        content = settings.load_content('telegraf', 'config')
        self.assertEqual(content, self._get_telegraf_conf_example())

    @mock.patch('scotty.core.settings.ConfigFile.path', new_callable=mock.PropertyMock)
    def test_scotty_namespace_missing(self, config_file_path):
        """Run test for missing scotty namespace."""
        config_path = self._get_scotty_conf_path('scotty-without-namespace.conf')
        config_file_path.return_value = config_path
        settings.load_settings()
        namespace = settings.scotty_namespace()
        self.assertEqual(namespace, 'default')

    @mock.patch('scotty.core.settings.ConfigFile.path', new_callable=mock.PropertyMock)
    def test_scotty_namespace(self, config_file_path):
        """Run test for missing scotty namespace."""
        config_path = self._get_scotty_conf_path('scotty.conf')
        config_file_path.return_value = config_path
        settings.load_settings()
        namespace = settings.scotty_namespace()
        self.assertEqual(namespace, 'scotty_example_namespace')

    @mock.patch('scotty.core.settings.ConfigFile.path', new_callable=mock.PropertyMock)
    def test_influxdb(self, config_file_path):
        """Run test for example scotty configuration."""
        config_path = self._get_scotty_conf_path('scotty.conf')
        config_file_path.return_value = config_path
        settings.load_settings()
        influxdb_database = settings.get('influxdb', 'database')
        influxdb_username = settings.get('influxdb', 'username')
        influxdb_password = settings.get('influxdb', 'password')
        influxdb_host = settings.get('influxdb', 'host')
        influxdb_ssl = settings.get('influxdb', 'ssl')
        influxdb_verify_ssl = settings.get('influxdb', 'verify_ssl')
        influxdb_port = settings.get('influxdb', 'port')
        self.assertEqual(influxdb_database, 'scotty')
        self.assertEqual(influxdb_username, 'scotty')
        self.assertEqual(influxdb_password, 'secred')
        self.assertEqual(influxdb_host, 'localhost')
        self.assertEqual(influxdb_ssl, True)
        self.assertEqual(influxdb_verify_ssl, False)
        self.assertEqual(influxdb_port, 8086)

    def _get_scotty_conf_path(self, config_file):
        here = os.path.abspath(os.path.dirname(__file__))
        here = os.path.join(here, '../../../../')
        here = os.path.normpath(here)
        return os.path.join(here, f'etc/{config_file}')

    def _get_telegraf_conf_example(self):
        path = self._get_scotty_conf_path('telegraf.conf')
        content = ''
        with open(path, 'r') as f:
            content = f.read()
        return content
