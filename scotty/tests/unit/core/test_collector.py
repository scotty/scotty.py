import unittest

import mock

from scotty.core.collector import TelegrafResourceCollector


class CollectorTest(unittest.TestCase):
    """Testclass for core collector."""

    @mock.patch('scotty.core.collector.TelegrafResourceCollector._load_setting_config')
    def test_telegraf_resource_collector_create_config_with_tags(self, mock_load_setting_config):
        """Run test for telegraf resource collector create config with [global_tags]'."""
        mock_load_setting_config.return_value = self._create_telegraf_conf_with_global_tags()
        telegraf_resource_collector = TelegrafResourceCollector()
        telegraf_conf = telegraf_resource_collector.create_config(
            'test_experiment',
            '123456789',
            'test_resource'
        )
        telegraf_conf_compare = self._create_telegraf_conf_with_scotty_tags(
            'test_experiment',
            '123456789',
            'test_resource'
        )
        self.assertEqual(telegraf_conf, telegraf_conf_compare)

    @mock.patch('scotty.core.collector.TelegrafResourceCollector._load_setting_config')
    def test_telegraf_resource_collector_create_config(self, mock_load_setting_config):
        """Run test for telegraf resource collector create config without [global_tags]'."""
        mock_load_setting_config.return_value = self._create_telegraf_conf_without_global_tags()
        telegraf_resource_collector = TelegrafResourceCollector()
        telegraf_conf = telegraf_resource_collector.create_config(
            'test_experiment',
            '123456789',
            'test_resource'
        )
        telegraf_conf_compare = self._create_telegraf_conf_with_scotty_tags(
            'test_experiment',
            '123456789',
            'test_resource'
        )
        self.assertEqual(telegraf_conf, telegraf_conf_compare)

    def _create_telegraf_conf_with_scotty_tags(self, exp_name, exp_uuid, resource_name):
        telegraf_conf = ''
        telegraf_conf += '[global_tags]\n'
        telegraf_conf += f'  experiment_name = "{exp_name}"\n'
        telegraf_conf += f'  experiment_uuid = "{exp_uuid}"\n'
        telegraf_conf += f'  resource_name = "{resource_name}"\n'
        telegraf_conf += '\n'
        telegraf_conf += self._create_telegraf_conf_without_global_tags()
        return telegraf_conf

    def _create_telegraf_conf_without_global_tags(self):
        telegraf_conf = ''
        telegraf_conf += '[agent]\n'
        telegraf_conf += '  interval = "5s"\n'
        telegraf_conf += '  round_interval = true\n'
        telegraf_conf += '\n'
        telegraf_conf += '[[outputs.influxdb]]\n'
        telegraf_conf += '  database = "scotty"\n'
        return telegraf_conf

    def _create_telegraf_conf_with_global_tags(self):
        telegraf_conf = ''
        telegraf_conf += '[global_tags]\n'
        telegraf_conf += '\n'
        telegraf_conf += self._create_telegraf_conf_without_global_tags()
        return telegraf_conf
