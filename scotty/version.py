import pbr.version

__version__ = pbr.version.VersionInfo('scotty').release_string()
