import logging

from scotty.utils.base import BaseUtils


logger = logging.getLogger(__name__)


class ExperimentUtils(BaseUtils):
    """Utils class for experiments."""

    pass
