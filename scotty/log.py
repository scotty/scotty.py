import inspect
import logging
from functools import wraps

from scotty.core import settings


def _setup_file_handler(logger, log_settings):
    file_handler = logging.FileHandler(log_settings['path'])
    file_handler.setFormatter(logging.Formatter(log_settings['format']))
    logger.addHandler(file_handler)


def _setup_stream_handler(logger, log_settings):
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(logging.Formatter(log_settings['format']))
    logger.addHandler(stream_handler)


def _load_logger_settings():
    log_settings = {}
    log_dir = settings.get('logging', 'log_dir', True)
    log_file = settings.get('logging', 'log_file')
    log_settings['path'] = log_dir + '/' + log_file
    log_settings['format'] = settings.get('logging', 'log_format')
    log_settings['level'] = settings.get('logging', 'log_level')
    return log_settings


def setup_logging():
    """Setups logging with settings from scotty.conf."""
    log_settings = _load_logger_settings()
    logger = logging.getLogger('scotty')
    logger.setLevel(log_settings['level'].upper())
    _setup_file_handler(logger, log_settings)
    _setup_stream_handler(logger, log_settings)


def getLogger(name):  # noqa: N802
    """Return logger with scotty.customer prefix."""
    return logging.getLogger(f'scotty.customer.{name}')


def _warning_with_source(msg, stack_level):
    logger = logging.getLogger('scotty')
    stack = inspect.stack()[stack_level]
    code_context = stack.code_context[0].strip()
    source_info = f'{stack.filename}:{stack.lineno}: {code_context}'
    logger.warning(f'{msg}\n {source_info}')


def deprecate(*params):
    """Wrap function to log a deprecation warning for given parameter."""
    def deprecate_decorator(func):
        @wraps(func)
        def wrapped_function(*args, **kwargs):
            for param in params:
                arg_value = _get_arg_value(func, param, *args)
                if arg_value:
                    _send_param_deprecate_message(param)
            return func(*args, **kwargs)
        return wrapped_function
    return deprecate_decorator


def _send_param_deprecate_message(param):
    msg = f'DeprecationWarning: {param}'
    msg = f'{msg} deprecated and will be deleted in future'
    _warning_with_source(msg, 3)
    return msg


def _get_arg_value(func, parameter, *args):
    arg_spec = inspect.getfullargspec(func)
    arg_index = arg_spec.args.index(parameter)
    arg_value = None
    if arg_index:
        if len(args) > arg_index:
            arg_value = args[arg_index]
    return arg_value
