*.pyc
/log/*.log
*.swp
*.key
*.pub

# Unit test / coverage reports
.coverage
.coverage.*
.pytest_cache/

.cache/

.scotty/

# tests
tmp
